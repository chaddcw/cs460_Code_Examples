
TARGETS=bin/dup2_example bin/simplePipeTest  bin/pipeTest bin/pipeBug bin/exec_example bin/shm_fork_example bin/libmakeMyPidZero.so bin/a bin/printArgv

CFLAGS=-g -Wall

all: ${TARGETS}

bin/dup2_example: bin/dup2_example.o
	gcc -o bin/dup2_example bin/dup2_example.o -g
	
bin/dup2_example.o: src/dup2_example.c
	gcc -o bin/dup2_example.o -c src/dup2_example.c ${CFLAGS}
	
	
bin/simplePipeTest: bin/simplePipeTest.o
	gcc -o $@ -g $< 

bin/simplePipeTest.o: src/simplePipeTest.c
	gcc -o $@ -c ${CFLAGS} $< 
	

bin/pipeTest: bin/pipeTest.o
	gcc -o $@ -g $< 

bin/pipeTest.o: src/pipeTest.c
	gcc -o $@ -c  ${CFLAGS} $< 

bin/pipeBug: bin/pipeBug.o
	gcc -o $@ -g $< 

bin/pipeBug.o: src/pipeBug.c
	gcc -o $@ -c  ${CFLAGS} $< 
	
bin/exec_example: bin/exec_example.o
	gcc -o $@ -g $< 

bin/exec_example.o: src/exec_example.c
	gcc -o $@ -c  ${CFLAGS} $< 

bin/shm_fork_example: bin/shm_fork_example.o
	gcc -o $@ -g $< 

bin/shm_fork_example.o: src/shm_fork_example.c
	gcc -o $@ -c  ${CFLAGS} $< 

bin/a: bin/a.o
	gcc -o $@ -g $< 

bin/a.o: src/a.c
	gcc -o $@ -c  ${CFLAGS} $< 

bin/printArgv: bin/printArgv.o
	gcc -o $@ -g $< 

bin/printArgv.o: src/printArgv.c
	gcc -o $@ -c  ${CFLAGS} $< 


bin/makeMyPidZero.o: src/makeMyPidZero.c
	gcc -o $@ -c  ${CFLAGS} $< 


bin/libmakeMyPidZero.so: bin/makeMyPidZero.o
	gcc -shared -o $@ $<
# thanks, Shereen.
# export LD_PRELOAD=./libmakeMyPidZero.so
	
clean:
	rm -rf bin/*.o ${TARGETS}

	

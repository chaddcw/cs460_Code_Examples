#!/bin/bash

########################################################################
# File name:  
# Author:
# Date:       
# Class:      
# Assignment:  
# Purpose:    
########################################################################


########################################################################
# Function:    
# Description: 
# Parameters:  $1 - 
# Returned:    
########################################################################


########################################################################
# Section: 
########################################################################

# Example if statement
if [ 1 -eq 1 ]
then 	# note then is on its own, unidented line

	; 	# note statements in the block are indented
	
elif [ 1 -ne 0 ]
then	# note then is on its own, unidented line

	; 	# note statements in the block are indented
	
else

	; 	# note statements in the block are indented
	
fi 		# fi is also unindented


# Example while statement
while [ 1 -ne 1 ]
do		# note do is on its own, unidented line

	; 	# note statements in the block are indented

done	# done is also unindented

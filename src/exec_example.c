//****************************************************************************
// File name:		exec_example.c
// Author:			chadd williams
// Date:				2/21/2018
// Class:				CS 460
// Assignment:	In Class Examples
// Purpose:			Demonstrate fork/exec
//***************************************************************************


#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

/****************************************************************************
 Function: 		main
 
 Description:	fork then exec in child
 
 Parameters:	none
 
 Returned:		0
****************************************************************************/
int main() 
{
	pid_t  pid;
	int value = 0;
	
	value = 9;
	
	/* fork another process */
	pid = fork();
	
	fprintf(stderr,"The value: %d pid: %d\n\n", value, pid);
	
	if (pid < 0)
	{ 
		/* error occurred */
		fprintf(stderr, "Fork Failed");
		exit(-1);
	}
	else if (0 == pid)
	{ 
		/* I AM A CHILD */
		fprintf(stderr,"The value: %d pid: %d\n\n", value, pid);

		value = 10;

		fprintf(stderr,"The value: %d pid: %d\n\n", value, pid);

		
		execlp("/usr/bin/echo", "echo", "HELLO", NULL);
	}
	else 
	{
		/* I AM A PARENT */
		wait(NULL);
		printf ("\n\nChild Complete value: %d pid: %d\n\n", value, pid);
	}
	return 0;
}


//****************************************************************************
// File name:		shm_fork_example.c
// Author:			chadd williams
// Date:				2/21/2018
// Class:				CS 460
// Assignment:	In Class Examples
// Purpose:			Demonstrate shared memory with fork
//***************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>

/****************************************************************************
 Function:		main
 
 Description:	create shared memory, fork, write to shm in parent, 
							read from shm in child.
 
 Parameters:	none
 
 Returned:		0
****************************************************************************/

int main()
{
	int segment_id;
	char *shared_memory;
	
	const long size = sysconf(_SC_PAGESIZE);
	
	pid_t pid;
	
	/* allocate shared memory segment */
	segment_id = shmget (IPC_PRIVATE, size, S_IRUSR | S_IWUSR);
	
	/* attach the shared memory segment */
	shared_memory = (char*) shmat (segment_id, NULL, 0);

	pid = fork();
	
	if ( 0 != pid)
	{
		/* Write to shared memory */
		sprintf(shared_memory, "Hello CS 460!");

		/* for child to terminate */
		wait(NULL);

		/* detach from shared memroy */
		/* the child should have already detached before termination */
		shmdt(shared_memory);
		
		/* remove shared memory */
		shmctl(segment_id, IPC_RMID, NULL);
	
	}
	else
	{
		/* wait for parent to write */
		/* hacky synchronization */
		sleep (2);
		
		/* read/print from shared segment */
		printf("MESSAGE: %s\n", shared_memory);

		/* detach from shared memroy */
		shmdt(shared_memory);
	}
		
	return 0;
}

/****************************************************************************
 File name:  makeMyPidZero.c
 Author:     chadd williams
 Date:       March 7, 2018
 Class:      CS 460
 Assignment: Shell
 Purpose:    hide getpid() and return 0
 ****************************************************************************/

#include <stdio.h>

/****************************************************************************
Function: 		getpid

Description: 	always returns zero

Parameters: 	none

Returned: 		int - always returns zero
****************************************************************************/

// thanks, Shereen.
// export LD_PRELOAD=./libmakeMyPidZero.so

int getpid()
{
	return 0;
}

//****************************************************************************
// File name:		pipeTest.c
// Author:			chadd williams
// Date:				2/21/2018
// Class:				CS 460
// Assignment:	In Class Examples
// Purpose:			Demonstrate pipe with fork
//***************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define MAXLEN 1024
#define READ 0
#define WRITE 1

/****************************************************************************
 Function: 		readFromCommandLine
 
 Description:	read a string from stdin
 
 Parameters:	data - buffer to read into
							maxSize - size of buffer
 
 Returned:		none
****************************************************************************/
	
void readFromCommandLine(char * data, int maxSize)
{
	memset(data,'\0', maxSize);
	fprintf(stderr,"> ");
	fgets(data, maxSize, stdin);
}

/****************************************************************************
 Function: 		main
 
 Description:	create a pipe, fork, then send data from parent to child
 
 Parameters:	none
 
 Returned:		0
****************************************************************************/

int main()
{
  /* pipe(int filedes[2])  creates  a  pair of file 
   * descriptors, pointing to a pipe inode, and places 
   * them in the array pointed to by filedes.  
   * filedes[0] is for reading, 
   * filedes[1] is for writing.
   */

	char data[MAXLEN];
	int thePipe[2];
	pid_t childPid;
	
	memset(&(data[0]), '\0', MAXLEN);
	
	pipe(thePipe);
	
	childPid = fork();
	
	if(0 == childPid)
	{
		/* I AM A CHILD */
		close(thePipe[WRITE]);
		read(thePipe[READ], &(data[0]), MAXLEN);
		
		while(strncmp( &(data[0]), "STOP", 4) != 0 )
		{
			printf("CHILD> |%s|\n", &(data[0]));
			read(thePipe[READ], &(data[0]), MAXLEN);
		}
		close(thePipe[READ]);	
	}
	else
	{
		close(thePipe[READ]);

		readFromCommandLine(&(data[0]), MAXLEN);
		
		write(thePipe[WRITE], &(data[0]), strlen(data)+1);
		
		while(strncmp(&(data[0]), "STOP", 4) != 0)
		{
			//fprintf(stderr,"> ");
			readFromCommandLine(&(data[0]), MAXLEN);
			//fgets(&(data[0]), MAXLEN, stdin);
			write(thePipe[WRITE], &(data[0]), strlen(data)+1);
		}
		close(thePipe[WRITE]);
	}
	
	return 0;
}

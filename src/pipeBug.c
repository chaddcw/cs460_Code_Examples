//****************************************************************************
// File name:		pipeBug.c
// Author:			chadd williams
// Date:				2/21/2018
// Class:				CS 460
// Assignment:	In Class Examples
// Purpose:			Demonstrate a common bug with using pipe
//***************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#define MAXSIZE 1024
#define WRITE 1
#define READ 0

/****************************************************************************
 Function: 		readFromCommandLine
 
 Description:	read a string from stdin
 
 Parameters:	data - buffer to read into
							maxSize - size of buffer
 
 Returned:		none
****************************************************************************/

void readFromCommandLine(char *data, int maxSize) {
	fprintf(stderr,"> ");
	fgets(data, maxSize, stdin);
}

/****************************************************************************
 Function: 		main
 
 Description:	create a pipe, fork, then send data from parent to child
							CONTAINS A BUG!
 
 Parameters:	none
 
 Returned:		0
****************************************************************************/

int main() 
{
	/* Find the bug in this code
	 * What is it trying to do?
	 * What erroneous behavior does it exhibit?
	 * Why
	 */
	char data[MAXSIZE + 1];
	int thePipe[2];
	pid_t childPid;
	int status;

	pipe(thePipe);
	childPid = fork();
	if (childPid == 0) 
	{
		
		/* I AM A CHILD */
		
		while (read(thePipe[READ], data, MAXSIZE) > 0) 
		{
			printf("CHILD> %s\n", data);
		}
		close(thePipe[READ]);
	} 
	else 
	{
		
		/* parent */

		readFromCommandLine(data, MAXSIZE);
		while (strncmp(data, "STOP", 4) != 0) 
		{
			write(thePipe[WRITE], data, strlen(data)+1);
			readFromCommandLine(data, MAXSIZE);
		}
		close(thePipe[WRITE]);
		waitpid(childPid, &status, 0);
	}
	
	return 0;
}

/****************************************************************************
 File name:  printArgv.c
 Author:     chadd williams
 Date:       March 14, 2018
 Class:      CS 460
 Assignment: Shell
 Purpose:    Print the command line arguments
 ****************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

/****************************************************************************
Function: 		main

Description: 	print all the command line arguments

Parameters: 	argc - argument counts
							argv - array of char* that are the arguments

Returned: 		int - 0
****************************************************************************/

int main(int argc, char* argv[])
{
	int count;
	
	for(count = 0 ; count < argc ; count ++)
	{
		fprintf(stderr, "[%d] %s\n", count ,argv[count]);
	}

	return 0;
}

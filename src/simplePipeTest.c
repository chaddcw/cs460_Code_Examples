//****************************************************************************
// File name:		simplePipeTest.c
// Author:			chadd williams
// Date:				2/21/2018
// Class:				CS 460
// Assignment:	In Class Examples
// Purpose:			Demonstrate a pipe without fork
//***************************************************************************


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define MAXLEN 1024
#define READ 0
#define WRITE 1
		
/****************************************************************************
 Function: 		readFromCommandLine
 
 Description:	read a string from stdin
 
 Parameters:	data - buffer to read into
							maxSize - size of buffer
 
 Returned:		none
****************************************************************************/

void readFromCommandLine(char * data, int maxSize)
{
	memset(data,'\0', maxSize);
	fprintf(stderr,"> ");
	fgets(data, maxSize, stdin);
}

/****************************************************************************
 Function: 		main
 
 Description:	create a pipe then write and read data from the pipe
 
 Parameters:	none
 
 Returned:		0
****************************************************************************/

int main()
{
  /* pipe(int filedes[2])  creates  a  pair of file 
   * descriptors, pointing to a pipe inode, and places 
   * them in the array pointed to by filedes.  
   * filedes[0] is for reading, 
   * filedes[1] is for writing.
   */

	char dataPipeWrite[MAXLEN];
	char dataPipeRead[MAXLEN];

	int thePipe[2];
	
	memset(&(dataPipeWrite[0]), '\0', MAXLEN);
	memset(&(dataPipeRead[0]), '\0', MAXLEN);
	
	pipe(thePipe);
	
	readFromCommandLine(dataPipeWrite,MAXLEN);
	
	write(thePipe[WRITE], &(dataPipeWrite[0]), strlen(dataPipeWrite)+1);

	read(thePipe[READ], &(dataPipeRead[0]), MAXLEN);

	fprintf(stderr,"READ FROM PIPE: %s\n",&(dataPipeRead[0]));
	
	close(thePipe[WRITE]);
	close(thePipe[READ]);
	
	return 0;
}

/****************************************************************************
 File name:  a.c
 Author:     chadd williams
 Date:       March 7, 2018
 Class:      CS 460
 Assignment: Shell
 Purpose:    build a small command that will accept an int from argv or
							stdin and print, to stdout, int+1
 ****************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

/****************************************************************************
Function: 		main

Description: 	accept an int from argv or
							stdin and print, to stdout, int+1

Parameters: 	argc - argument counts
							argv - array of char* that are the arguments

Returned: 		int - 0
****************************************************************************/

int main(int argc, char* argv[])
{
	
	char inputBuffer[100];
	int charRead = 0;
	int value = 1;

	if( 2 == argc )
	{
		value = atoi( argv[1] );
	}
	
	else
	{
		charRead = read(STDIN_FILENO, inputBuffer, 99);
		if( 0 < charRead )
		{
			inputBuffer[charRead] = '\0';
			value = atoi(inputBuffer)  + 1;
		}
	}
	printf("%d", value);
	
	return 0;
}
